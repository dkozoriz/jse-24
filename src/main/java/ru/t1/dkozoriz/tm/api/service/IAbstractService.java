package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IAbstractService<T> {

    @NotNull
    T findById(@Nullable String id);

    @Nullable
    T removeById(@Nullable String id);

    void clear();

    @NotNull
    T add(@Nullable T user);

    @NotNull
    List<T> findAll();

    @Nullable
    T remove(@Nullable T user);

}