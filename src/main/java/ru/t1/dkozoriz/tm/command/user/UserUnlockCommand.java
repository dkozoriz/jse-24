package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    public UserUnlockCommand() {
        super("user-unlock", "user unlock.");
    }

    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        getUserService().unLockUserByLogin(login);
    }
}
