package ru.t1.dkozoriz.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.dkozoriz.tm.api.repository.ICommandRepository;
import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.*;
import ru.t1.dkozoriz.tm.api.service.business.IProjectService;
import ru.t1.dkozoriz.tm.api.service.business.ITaskService;
import ru.t1.dkozoriz.tm.command.AbstractCommand;
import ru.t1.dkozoriz.tm.command.project.*;
import ru.t1.dkozoriz.tm.command.system.*;
import ru.t1.dkozoriz.tm.command.task.*;
import ru.t1.dkozoriz.tm.command.user.*;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dkozoriz.tm.exception.system.CommandNotSupportedException;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.repository.CommandRepository;
import ru.t1.dkozoriz.tm.repository.UserRepository;
import ru.t1.dkozoriz.tm.repository.business.ProjectRepository;
import ru.t1.dkozoriz.tm.repository.business.TaskRepository;
import ru.t1.dkozoriz.tm.service.*;
import ru.t1.dkozoriz.tm.service.business.ProjectService;
import ru.t1.dkozoriz.tm.service.business.TaskService;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMAND = "ru.t1.dkozoriz.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMAND);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private boolean processArguments(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length < 1) return false;
        processArgument(arguments[0]);
        return true;
    }

    private void processCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private void initLogger() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("*** TASK MANAGER IS SHUTTING DOWN ***");
            }
        });
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() {
        @NotNull final User user1 = userService.create("user1", "password1", "user1@user");
        @NotNull final User user2 = userService.create("user2", "password2", "user2@user");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        @NotNull final Project project1 = new Project("Project1", Status.IN_PROGRESS);
        @NotNull final Project project2 = new Project("Project2", Status.IN_PROGRESS);
        @NotNull final Project project3 = new Project("Project3", Status.COMPLETED);
        @NotNull final Project project4 = new Project("Project4", Status.COMPLETED);
        @NotNull final Project project5 = new Project("Project5", Status.COMPLETED);
        @NotNull final Project project6 = new Project("Project6", Status.IN_PROGRESS);
        projectRepository.add(user1.getId(), project1);
        projectRepository.add(user1.getId(), project2);
        projectRepository.add(user1.getId(), project3);
        projectRepository.add(user2.getId(), project4);
        projectRepository.add(user2.getId(), project5);
        projectRepository.add(user2.getId(), project6);

        taskService.create(user1.getId(), "task1_1", "description", project1.getId());
        taskService.create(user1.getId(), "task1_2", "description", project1.getId());
        taskService.create(user1.getId(), "task2_1", "description", project2.getId());
        taskService.create(user1.getId(), "task3_1", "description", project3.getId());
        taskService.create(user1.getId(), "task3_2", "description", project3.getId());
    }

    public void run(@Nullable final String... args) {
        if (processArguments(args)) return;
        initLogger();
        initData();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @Nullable final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]\n");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]\n");
            }
        }
    }

}