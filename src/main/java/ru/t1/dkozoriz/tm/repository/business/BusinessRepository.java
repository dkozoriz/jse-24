package ru.t1.dkozoriz.tm.repository.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.api.repository.business.IBusinessRepository;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;
import ru.t1.dkozoriz.tm.repository.UserOwnedRepository;

import java.util.Comparator;
import java.util.List;

public abstract class BusinessRepository<T extends BusinessModel> extends UserOwnedRepository<T>
        implements IBusinessRepository<T> {

    @Nullable
    public List<T> findAll(@Nullable final String userId, @NotNull final Comparator<? super IWBS> comparator) {
        if (userId == null) return null;
        @NotNull final List<T> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

}